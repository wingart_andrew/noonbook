$(document).ready(function(){
	$('.menu_click_btn').on('click',function(){
		var $this = $(this),
			parent = $this.parents('.menu_click');
		if (parent.hasClass('active')) {
			parent.removeClass('active');
		}
		else{
			$('.menu_click').removeClass('active');
			parent.addClass('active');
		}
	});

	$('select').styler();

		// enter

	$('.enter_tabs_link').on('click',function(){
		var idTab = $(this).attr('href');
		$(idTab).addClass('active').siblings('.enter_tabs').removeClass('active');
	});	

		// box_noonbook_15
	$('#link').on('click',function(){
		$(this).toggleClass('active');
		$('.bg_noonbook_15').toggleClass('active');
	});	

});

